package testing;

import java.util.Date;

import exceptionHandling.exceptionHandler;
import junit.framework.Assert;
import junit.framework.TestCase;
import production.Admin;

public class AdminTest extends TestCase {
	
	int employee, publication, customer, quantity;
	Date date;
	boolean expected;

	Admin admin = new Admin();
	// Create Admin object

	// Test No: 1
	// Object: change publication price
	// Inputs: name = "May", price ="8.0"
	// Expected Output: return true
	public void testchangePublicationPrice1() {
		// Test the method
		assertEquals(true, admin.changePublicationPrice("May", (float) 8.0));
	}

	// Test No: 2
	// Object: change publication price
	// Inputs: name = "ABC", price ="5.0"
	// Expected Output: return true
	public void testchangePublicationPrice2() {
		// Test the method
		assertEquals(true, admin.changePublicationPrice("ABC", (float) 5.0));
	}

	// Test No: 3
	// Object: change publication price
	// Inputs: name = "News", price ="0.0"
	// Expected Output: return false
	public void testchangePublicationPrice3() {
		// Test the method
		assertEquals(false, admin.changePublicationPrice("News", (float) 0.0));
	}

	// Test No: 4
	// Object: list the most popular publication ordered by price
	// Expected Output: return true
	public void testmostpopularList() {
		// Test the method
		assertEquals(true, admin.mostpopularList());
	}

	// Test No:5
	// Test Objective: I can search summary by its date
	// Input:a date in the database
	// excepted output:True

	public void testPrintSummaryByDate1() {
		// Test the method
		assertEquals(true, admin.PrintSummaryByDate("2015-03-10"));
	}

	// Test No:6
	// Test Objective: I can not search the summary without a date
	// Input:non-existing date in the database
	// excepted output:False

	public void testPrintSummaryByDate2() {
		// Test the method
		assertEquals(false, admin.PrintSummaryByDate(""));
	}
	
	
	// ------------------------------------------------------------------------------------------
	// Gary User Story ADR6 Register Customer
	// Testing the add customer method

	// Test 001
	// Objective: Test to catch exception when first name String is not entered.
	// Inputs: "", surname,street,number,segment,town,phone number.
	// Outputs: Catch exception First Name Missing.
	public void testAddCustomer001() {
		admin= new Admin();
		try {
			admin.addCustomer("", "surname", "street", 24, 1, "Athlone", "123456789");
			fail("Should not get here");
		} catch (exceptionHandler e) {
			assertSame("First Name Missing.", e.getMessage());
		}
	}

	// Test 002
	// Objective: Test to catch exception when surname String is not entered.
	// Inputs: firstName, "", street,number,segment,town,phone number.
	// Outputs: Catch exception Surname Missing.
	public void testAddCustomer002() {
		try { 
			admin.addCustomer("firstName", "", "street", 24, 1, "Athlone", "123456789");
			fail("Should not get here");
		} catch (exceptionHandler e) {
			assertSame("Surname Missing.", e.getMessage());
		}
	}

	// Test 003
	// Objective: Test to catch exception when street String is not entered.
	// Inputs: firstName, surname,"" ,number,segment,town,phone number.
	// Outputs: Catch exception Street Name Missing.
	public void testAddCustomer003() {
		try {
			admin.addCustomer("firstName", "surname", "", 24, 1, "Athlone", "123456789");
			fail("Should not get here");
		} catch (exceptionHandler e) {
			assertSame("Street Missing.", e.getMessage());
		}
	}

	// Test 004
	// Objective: Test to catch exception when number Integer is not entered.
	// Inputs: firstName, surname, street,null,segment,town,phone number.
	// Outputs: Catch exception Number Missing.
	public void testAddCustomer004() {
		try {
			admin.addCustomer("firstName", "surname", "street", 0, 1, "Athlone", "123456789");
			fail("Should not get here");
		} catch (exceptionHandler e) {
			assertSame("Number Missing.", e.getMessage());
		}
	}

	// Test 005
	// Objective: Test to catch exception when segment Integer is not entered.
	// Inputs: firstName, surname, street,24n,0,town,phone number.
	// Outputs: Catch exception Segment Missing.
	public void testAddCustomer005() {
		try {
			admin.addCustomer("firstName", "surname", "street", 24, 0, "Athlone", "123456789");
			fail("Should not get here");
		} catch (exceptionHandler e) {
			assertSame("Value is Invalid.", e.getMessage());
		}

	}

	// Test 006
	// Objective: Test to catch exception when town is not entered.
	// Inputs: firstName, surname, street,number,segment,"",phone number.
	// Outputs: Catch exception Town Missing.
	public void testAddCustomer006() {
		try {
			admin.addCustomer("firstName", "surname", "street", 24, 1, "", "123456789");
			fail("Should not get here");
		} catch (exceptionHandler e) {
			assertSame("Town Missing.", e.getMessage());
		}

	}

	// Test 007
	// Objective: Test to catch exception when phone number is not entered.
	// Inputs: firstName, surname, street,number,segment,"",phone number.
	// Outputs: Catch exception Phone Number Missing.
	public void testAddCustomer007() {
		try {
			admin.addCustomer("firstName", "surname", "street", 24, 1, "Athlone", "");
			fail("Should not get here");
		} catch (exceptionHandler e) {
			assertSame("Phone Number Missing.", e.getMessage());
		}

	}

	// Test008
	// Objective: Validate that segment integer is value 1,2,3,4 when all valid
	// customer information entered.
	// Input: int 0 for segment value and all other values are correct.
	// Output Catch Exception Value is Invalid;
	public void testAddCustomer008() {
		try {
			admin.addCustomer("firstName", "surname", "Street", 24, 0, "Athlone", "123456789");
			fail("Should not get here");
		} catch (exceptionHandler e) {
			assertSame("Value is Invalid.", e.getMessage());
		}
	}

	// Test009
	// Objective:Validate that segment integer is value 1,2,3,4 when all valid
	// customer information entered.
	// Inputs: int 5 for segment value and all other values are correct.
	// Outputs:Catch Exception Value is Invalid
	public void testAddCustomer009() {
		try {
			admin.addCustomer("firstName", "surname", "Street", 24, 5, "Athlone", "123456789");
			fail("Should not get here");
		} catch (exceptionHandler e) {
			assertSame("Value is Invalid.", e.getMessage());
		}
	}

	// Test010
	// Objective: Verify that a the correct payment details are entered. Must be
	// cash or cheque.
	// Inputs: String Cash will be used.
	// Outputs: Return true if cash is entered.
	public void testAddPaymentDetails010() {
		try {
			assertEquals(true, admin.addPaymentDetails("Cash"));
		} catch (exceptionHandler e) {
			e.printStackTrace();
		}

	}

	// Test011
	// Objective: Verify that a the correct payment details are entered. Must be
	// cash or cheque.
	// Inputs: String Cheque will be used.
	// Outputs: Return true if cheque is entered.
	public void testAddPaymentDetails011() {
		try {
			assertEquals(true, admin.addPaymentDetails("Cheque"));
		} catch (exceptionHandler e) {
			e.printStackTrace();
		}
	}

	// Test012
	// Objective: Test to catch exception when cash or cheque is not entered.
	// Inputs: String anything is used to represent any word that is not Cash or
	// Cheque.
	// Outputs: Catch Exception Wrong Payment Type.
	public void testAddPaymentDetails012() {
		try {
			admin.addPaymentDetails("anything");
			fail("Should not get here");
		} catch (exceptionHandler e) {
			assertSame("Wrong Payment Type", e.getMessage());

		}
	}

	// Test 013
	// Objective check with all valid information
	// Inputs Tom, Dooley,Main Street,24,2,Athlone,123456789.
	// Outputs True;
	public void testAddCustomer013() {
		try {
			assertEquals(true, admin.addCustomer("Tom", "Dooley", "Main Street", 24, 2, "Athlone", "123456789"));
		} catch (exceptionHandler e) {
			e.printStackTrace();
		}
	}

	// ------------------------------------------------------------------------------------
	
	
	/*
	 * Test Id: AssignRoute1
	 * Inputs: employee=0, publication=1, customer=1, date=2016-03-03, quantity=1, db=true
	 * Expected output=false
	 * Objective: Test with an invalid employee
	 */
	
	public void testAssignRoute1(){
		
		employee = 0;
		publication=1;
		customer=1;
		quantity=1;
		date = new Date();
		date.setYear(2016);
		date.setMonth(3);
		date.setDate(3);	
		
		AdminDAOMock mock = new AdminDAOMock();
		mock.setBoolToReturn(true);
		FactoryAdminDAO.instance.setAdminDAO(mock);
		
		admin = new Admin(0, "", "");
		
		expected = false;
		
		Assert.assertEquals(expected, admin.assignRoute(employee, publication, customer, date, quantity));
	}
	


	/*
	 * Test Id: AssignRoute2
	 * Inputs: employee=1, publication=0, customer=1, date=2016-03-03, quantity=1, db=true
	 * Expected output=false
	 * Objective: Test with an invalid publication
	 */
	
	public void testAssignRoute2(){
		
		employee = 1;
		publication=0;
		customer=1;
		quantity=1;
		date = new Date();
		date.setYear(2016);
		date.setMonth(3);
		date.setDate(3);	
		
		
		AdminDAOMock mock = new AdminDAOMock();
		mock.setBoolToReturn(true);
		FactoryAdminDAO.instance.setAdminDAO(mock);
		admin = new Admin(0, "", "");
		
		expected = false;
		
		Assert.assertEquals(expected, admin.assignRoute(employee, publication, customer, date, quantity));
	}
	
	/*
	 * Test Id: AssignRoute3
	 * Inputs: employee=1, publication=1, customer=0, date=2016-03-03, quantity=1, db=true
	 * Expected output=false
	 * Objective: Test with an invalid customer
	 */
	
	public void testAssignRoute3(){
		
		employee = 1;
		publication=1;
		customer=0;
		quantity=1;
		date = new Date();
		date.setYear(2016);
		date.setMonth(3);
		date.setDate(3);	
		
		AdminDAOMock mock = new AdminDAOMock();
		mock.setBoolToReturn(true);
		FactoryAdminDAO.instance.setAdminDAO(mock);
		admin = new Admin(0, "", "");
		
		expected = false;
		
		Assert.assertEquals(expected, admin.assignRoute(employee, publication, customer, date, quantity));
	}
	
	/*
	 * Test Id: AssignRoute4
	 * Inputs: employee=1, publication=1, customer=1, date=null, quantity=1, db=true
	 * Expected output=false
	 * Objective: Test with a null date
	 */
	
	public void testAssignRoute4(){
		
		employee = 1;
		publication=1;
		customer=1;
		quantity=1;
		date = null;
		
		AdminDAOMock mock = new AdminDAOMock();
		mock.setBoolToReturn(true);
		FactoryAdminDAO.instance.setAdminDAO(mock);
		admin = new Admin(0, "", "");
		
		expected = false;
		
		Assert.assertEquals(expected, admin.assignRoute(employee, publication, customer, date, quantity));
	}
	
	/*
	 * Test Id: AssignRoute5
	 * Inputs: employee=1, publication=1, customer=1, date=2016-03-03, quantity=0, db=true
	 * Expected output=false
	 * Objective: Test with an invalid quantity
	 */
	
	public void testAssignRoute5(){
		
		employee = 1;
		publication=1;
		customer=1;
		quantity=0;
		date = new Date();
		date.setYear(2016);
		date.setMonth(3);
		date.setDate(3);	
		
		AdminDAOMock mock = new AdminDAOMock();
		mock.setBoolToReturn(true);
		FactoryAdminDAO.instance.setAdminDAO(mock);
		admin = new Admin(0, "", "");
		
		expected = false;
		
		Assert.assertEquals(expected, admin.assignRoute(employee, publication, customer, date, quantity));
	}
	
	/*
	 * Test Id: AssignRoute6
	 * Inputs: employee=1, publication=1, customer=1, date=2016-03-03, quantity=1, db=false
	 * Expected output=false
	 * Objective: Test with an error in the database
	 */
	
	public void testAssignRoute6(){
		
		employee = 1;
		publication=1;
		customer=1;
		quantity=1;
		date = new Date();
		date.setYear(2016);
		date.setMonth(3);
		date.setDate(3);	
		
		AdminDAOMock mock = new AdminDAOMock();
		mock.setBoolToReturn(false);
		FactoryAdminDAO.instance.setAdminDAO(mock);
		admin = new Admin(0, "", "");
		
		expected = false;
		
		Assert.assertEquals(expected, admin.assignRoute(employee, publication, customer, date, quantity));
	}
	
	/*
	 * Test Id: AssignRoute7
	 * Inputs: employee=1, publication=1, customer=1, date=2016-03-03, quantity=1, db=true
	 * Expected output=true
	 * Objective: Test with everything correct
	 */
	
	public void testAssignRoute7(){
		
		employee = 1;
		publication=1;
		customer=1;
		quantity=1;
		date = new Date();
		date.setYear(2016);
		date.setMonth(3);
		date.setDate(3);	
		
		AdminDAOMock mock = new AdminDAOMock();
		mock.setBoolToReturn(true);
		FactoryAdminDAO.instance.setAdminDAO(mock);
		admin = new Admin(0, "", "");
		
		expected = true;
		
		Assert.assertEquals(expected, admin.assignRoute(employee, publication, customer, date, quantity));
	}
	
	
//*******************************Adrian***************************//
	// test number 1
		// objectives: add publication to DB with name > 3 and price > 0
		// inputs: name=AIT Athlone
		// price=4.99f;
		// output=true

		public static void testAddPublication0001() {
			try {
				assertEquals(true, Admin.addPublication("AIT Athlone", 4.99f));
			} catch (Exception e) {
				fail("true expected");
			}
		}

		
		
		
		
		// test number 2
		// objectives: add publication to DB name < 3 char, price = 0
		// inputs: name=AI
		// price=0.0f;
		// output=false

		public static void testAddPublication0002() {
			try {
				assertEquals(false, Admin.addPublication("AI", 0.0f));
			} catch (Exception e) {
				fail("False expected");
			}
		}

		// test number 3
		// objectives: add publication to DB name = 3, price < 0
		// inputs: name=empty
		// price=-2.99;
		// output=false

		public static void testAddPublication0003() {
			try {
				assertEquals(false, Admin.addPublication("AIT", -2.99f));
			} catch (Exception e) {
				fail("False expected");
			}
		}

		// test number 4
		// objectives: add publication to DB name empty, price = 2.99
		// inputs: name=empty
		// price=2.99;
		// output=false

		public static void testAddPublication0004() {
			try {
				assertEquals(false, Admin.addPublication("", 2.99f));
			} catch (Exception e) {
				fail("False expected");
			}
		}

		// test number 5
		// objectives: remove publication
		// inputs: name = AIT Athlone
		// output: true

		public static void testRemovePublication0005() {
			try {
				assertEquals(1, Admin.removePublication("AIT Athlone"));

			} catch (Exception e) {
				fail("true expected");
			}

		}

		// test number 6
		// objectives: remove publication non existent
		// inputs: name = AIT Ath
		// output: false

		public static void testRemovePublication0006() {
			try {
				assertEquals(0, Admin.removePublication("AIT Ath"));

			} catch (Exception e) {
				fail("true expected");
			}

		}

		// test number 7
		// objectives: change days
		// inputs: name = 101, 1, 1, 1, 1, 1, 1, 1
		// output: 1

		public static void testalterCustomerDeliveryDay0007() {
			try {
				assertEquals(1, Admin.alterCustomerDeliveryDay(101, 1, 1, 1, 1, 1, 1, 1));

			} catch (Exception e) {
				fail("1 expected");
			}
		}

		// test number 8
		// objectives: change days
		// inputs: name = 102, 1, 1, 1, 1, 1, 1, 1
		// output: 0

		public static void testalterCustomerDeliveryDay0008() {
			try {
				assertEquals(0, Admin.alterCustomerDeliveryDay(102, 1, 1, 1, 1, 0, 1, 0));

			} catch (Exception e) {
				fail("0 expected");
			}
		}
	
		// Test No:1
		// Test Objective: customer must be remove
		// return true remove success
		// Input: id =1 exist in DB
		// excepted output:true
		public void testRemoveCustomer1() throws Exception {

			try {
				assertSame(true, admin.removeCustomer(1));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// Test No:2
		// Test Objective: customer must be remove according id
		// return false and move fail
		// Input: id =2 does exist
		// excepted output:false
		public void testRemoveCustomer2() throws Exception {
			
			
			try {
				assertSame(false, admin.removeCustomer(2));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// Test No:3
		// Test Objective: admin change max delivery cost
		// return true and change success
		// Input: cost=1
		// excepted output:true
		public void testchangeMaxDeliveryCost()  throws Exception {
		
			try {
				assertSame(true, admin.changeMaxDeliveryCost(1));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// Test No:4
		// Test Objective: Value must be a number, and no greater than one digit
		// return false and change fail
		// Input: cost=10
		// excepted output:false
		public void testchangeMaxDeliveryCost2() throws Exception  {
	
			try {
				assertSame(false, admin.changeMaxDeliveryCost(10));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// Test No:5
		// Test Objective: admin change max delivery cost
		// return true and change success
		// Input: cost=2
		// excepted output:true
		public void testchangeDeliveryCost()  throws Exception {
			try {
				assertSame(true, admin.changeDelivery(2));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// Test No:6
		// Test Objective: changeDeliveryCost Value must be a number, and no greater
		// than one digit
		// return false and change failed
		// Input: cost=10
		// excepted output:false
		public void testchangeDeliveryCost2()  throws Exception {
		
			try {
				assertSame(false, admin.changeDelivery(10));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// Test No:7
		// Test Objective: changeDeliveryCost Value must be a number, and no greater
		// than one digit
		// return false and change failed
		// Input: cost=-1
		// excepted output:false
		public void testchangeDeliveryCost3()  throws Exception {
			
			try {
				assertSame(false, admin.changeDelivery(-1));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		//// test change delivary cost according to publication quantity method
		
		   //Test No: 1
		   // Object: set price of each publication
		   // Input : quantity = 1      cost =0.5
	       // output :  true
		
		public void testDeliveryCost1(){
			// Test the quantity if it's meeting the right cost 
					assertEquals(true,Admin.changeDeliveryCost(1,  0.5));
			}
		
		    //Test No: 2
			// Object: set price of each publication
			// Input : quantity = 2      cost = 1
		    // output :  true
		public void testDeliveryCost2(){
			// Test the quantity if it's meeting the right cost 
				assertEquals(true,Admin.changeDeliveryCost(2,  1));
		}
		
		    //Test No: 3
			// Object: set price of each publication
			// Input : quantity = 3      cost =1.5
		    // output :  true
		public void testDeliveryCost3(){
			// Test the quantity if it's meeting the right cost 
				assertEquals(true,Admin.changeDeliveryCost(3,  1.5));
		}
		
		     //Test No: 4
		   	 // Object: set price of each publication
			 // Input : quantity = 6      cost =1.5
			 // output :  true
		public void testDeliveryCost4(){
			// Test the quantity if it's meeting the right cost
			
				assertEquals(true,Admin.changeDeliveryCost(6,  1.5));
		
		}
	
}


