package testing;

import java.util.Date;

import production.Admin;
import junit.framework.Assert;
import junit.framework.TestCase;

public class AdminDAOTest extends TestCase {
	
	int employee, publication, customer,quantity; 
	Date date;
	boolean expected;
	AdminDAO admin;
	
	/*
	 * Test Id: AssignRoute1
	 * Inputs: employee=0, publication=1, customer=1, date=2016-03-03, quantity=1
	 * Expected output=false
	 * Objective: Test with an invalid employee
	 */
	
	public void testAssignRoute1(){
		
		employee = 0;
		publication=1;
		customer=1;
		quantity=1;
		date = new Date();
		date.setYear(2016);
		date.setMonth(3);
		date.setDate(3);	
		
		
		admin = new AdminDAO();
		
		expected = false;
		
		Assert.assertEquals(expected, admin.assignRoute(employee, publication, customer, date, quantity));
	}
	
	/*
	 * Test Id: AssignRoute2
	 * Inputs: employee=1, publication=0, customer=1, date=2016-03-03, quantity=1
	 * Expected output=false
	 * Objective: Test with an invalid publication
	 */
	
	public void testAssignRoute2(){
		
		employee = 1;
		publication=0;
		customer=1;
		quantity=1;
		date = new Date();
		date.setYear(2016);
		date.setMonth(3);
		date.setDate(3);	
		
		
		admin = new AdminDAO();
		
		expected = false;
		
		Assert.assertEquals(expected, admin.assignRoute(employee, publication, customer, date, quantity));
	}
	
	/*
	 * Test Id: AssignRoute3
	 * Inputs: employee=1, publication=1, customer=0, date=2016-03-03, quantity=1
	 * Expected output=false
	 * Objective: Test with an invalid customer
	 */
	
	public void testAssignRoute3(){
		
		employee = 1;
		publication=1;
		customer=0;
		quantity=1;
		date = new Date();
		date.setYear(2016);
		date.setMonth(3);
		date.setDate(3);	
		
		
		admin = new AdminDAO();
		
		expected = false;
		
		Assert.assertEquals(expected, admin.assignRoute(employee, publication, customer, date, quantity));
	}
	
	/*
	 * Test Id: AssignRoute4
	 * Inputs: employee=1, publication=1, customer=1, date=null, quantity=1
	 * Expected output=false
	 * Objective: Test with a null date
	 */
	
	public void testAssignRoute4(){
		
		employee = 1;
		publication=1;
		customer=1;
		quantity=1;
		date = null;
		
		
		admin = new AdminDAO();
		
		expected = false;
		
		Assert.assertEquals(expected, admin.assignRoute(employee, publication, customer, date, quantity));
	}
	
	/*
	 * Test Id: AssignRoute5
	 * Inputs: employee=1, publication=1, customer=1, date=2016-03-03, quantity=0
	 * Expected output=false
	 * Objective: Test with an invalid quantity
	 */
	
	public void testAssignRoute5(){
		
		employee = 1;
		publication=1;
		customer=1;
		quantity=0;
		date = new Date();
		date.setYear(2016);
		date.setMonth(3);
		date.setDate(3);	
		
		
		admin = new AdminDAO();
		
		expected = false;
		
		Assert.assertEquals(expected, admin.assignRoute(employee, publication, customer, date, quantity));
	}
	

	
	/*
	 * Test Id: AssignRoute6
	 * Inputs: employee=1, publication=1, customer=1, date=2016-03-03, quantity=1
	 * Expected output=false
	 * Objective: Test with everything correct and the delivery should be created
	 */
	
	public void testAssignRoute6(){
		
		employee = 1;
		publication=1;
		customer=1;
		quantity=1;
		date = new Date();
		date.setYear(2016);
		date.setMonth(3);
		date.setDate(3);	
		
		
		admin = new AdminDAO();
		
		expected = true;
		
		Assert.assertEquals(expected, admin.assignRoute(employee, publication, customer, date, quantity));
	}
	
	
	/*
	 * Test Id: AssignRoute7
	 * Inputs: employee=1, publication=1, customer=1, date=2016-03-03, quantity=1
	 * Expected output=false
	 * Objective: Test with everything correct and with a delivery that already exists
	 */
	
	public void testAssignRoute7(){
		
		employee = 1;
		publication=1;
		customer=1;
		quantity=1;
		date = new Date();
		date.setYear(2016);
		date.setMonth(3);
		date.setDate(3);	
		
		
		admin = new AdminDAO();
		
		expected = true;
		
		Assert.assertEquals(expected, admin.assignRoute(employee, publication, customer, date, quantity));
	}
	
	

}
