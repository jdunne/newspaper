import static org.junit.Assert.*;

import org.junit.Test;


public class MainGUIcontent2Test {

	@Test
	// test1
	// test object valid first_name , surname, address, status, day, publication name .
	// input : first_name = "faris " , surname = "alateeq"  , address = "Athlone" , status = "true" , day ="Wed" , publication name ="Times"
	// expected : exception
	public void testDeliveryRoutes() {
		try {
			
			MainGUIcontent2.actionPerformed("faris ", "alateeq" , "Athlone","true","Wed","Times");
		} catch (Exception e1) {
			assertSame("status = true", e1.getMessage());
			e1.printStackTrace();
		}
	}	

}
