package testing;

import java.sql.SQLException;

import exceptionHandling.exceptionHandler;
import junit.framework.TestCase;
import production.DAO;
import production.GUI;

public class DAOTest extends TestCase {
	DAO dao = new DAO();

	// Test001
	// Objective: check databases when all information is correct.
	// Inputs: Tom,Dooley,Main Street,24,2,Athlone,123456789.
	// Output: True
	public void testRegisterValidation001() {

		try { 
			GUI gui = new GUI();
			assertEquals(true, dao.registerValidation("Tom", "Dooley", "Main Street", 24, 2, "Athlone", "123456789"));
		} catch (exceptionHandler | SQLException e) {
			e.printStackTrace();
		}
	}

	// Test002
	// Objective: check databases when all information is incorrect
	// Inputs: Bill,Jones,Not Main Street,30,55,Athl,46744.
	// Output: True
	public void testRegisterValidation002() {

		try {
			assertEquals(false, dao.registerValidation("Bill", "Jones", "Not Main Street", 30, 55, "Athl", "46744"));
		} catch (exceptionHandler | SQLException e) {
			e.printStackTrace();
		}
	}

	// Test003
	// Objective: check databases when first name is incorrect.
	// Inputs: Bill,Dooley,Main Street,24,2,Athlone,123456789.
	// Output: True
	public void testRegisterValidation003() {
		try {
			assertEquals(false, dao.registerValidation("Bill", "Dooley", "Main Street", 24, 2, "Athlone", "123456789"));
		} catch (exceptionHandler | SQLException e) {
			e.printStackTrace();
		}
	}

	// Test004
	// Objective: check databases when surname is incorrect.
	// Inputs: Tom,Jones,Main Street,24,2,Athlone,123456789.
	// Output: True
	public void testRegisterValidation004() {
		try {
			assertEquals(false, dao.registerValidation("Bill", "Jones", "Main Street", 24, 2, "Athlone", "123456789"));
		} catch (exceptionHandler | SQLException e) {
			e.printStackTrace();
		}
	}

	// Test005
	// Objective: check databases when street is incorrect.
	// Inputs: Bill,Dooley,Not Main Street,24,2,Athlone,123456789.
	// Output: True
	public void testRegisterValidation005() {
		try {
			assertEquals(false,
					dao.registerValidation("Tom", "Dooley", "Not Main Street", 24, 2, "Athlone", "123456789"));
		} catch (exceptionHandler | SQLException e) {
			e.printStackTrace();
		}
	}

	// Test006
	// Objective: check databases when number is incorrect.
	// Inputs: Bill,Dooley,Not Main Street,30,2,Athlone,123456789.
	// Output: True
	public void testRegisterValidation006() {
		try {
			assertEquals(false, dao.registerValidation("Tom", "Dooley", "Main Street", 30, 2, "Athlone", "123456789"));
		} catch (exceptionHandler | SQLException e) {
			e.printStackTrace();
		}
	}

	// Test007
	// Objective: check databases when segment is incorrect.
	// Inputs: Bill,Dooley,Not Main Street,24,5,Athlone,123456789.
	// Output: True
	public void testRegisterValidation007() {
		try {
			assertEquals(false, dao.registerValidation("Tom", "Dooley", "Main Street", 30, 5, "Athlone", "123456789"));
		} catch (exceptionHandler | SQLException e) {
			e.printStackTrace();
		}
	}

	// Test008
	// Objective: check databases when town is incorrect.
	// Inputs: Bill,Dooley,Not Main Street,24,2,Roscommon,123456789.
	// Output: True
	public void testRegisterValidation008() {
		try {
			assertEquals(false,
					dao.registerValidation("Bill", "Dooley", "Main Street", 30, 2, "Roscommon", "123456789"));
		} catch (exceptionHandler | SQLException e) {
			e.printStackTrace();
		}
	}

	// Test009
	// Objective: check databases when phone number is incorrect.
	// Inputs: Bill,Dooley,Not Main Street,24,2,Athlone,987654321.
	// Output: True
	public void testRegisterValidation009() {
		try {
			assertEquals(false, dao.registerValidation("Tom", "Dooley", "Main Street", 30, 2, "Athlone", "987654321"));
		} catch (exceptionHandler | SQLException e) {
			e.printStackTrace();
		}
	}

	// Testing the updateCustomerMethod
	// Test010
	// Objective: Test to catch exception when firstname is not entered.
	// Inputs: "","Dooley",1,1,1,1,1,1,1
	// Output: Catch exception firstName invalid.
	public void testUpdateCustomerDaysOfWeek001() {
		try {
			dao.updateCustomerDaysOfWeek("", "Dooley", 1, 1, 1, 1, 1, 1, 1);
			fail("Should Not Get Here");
		} catch (exceptionHandler e) {
			assertSame("firstName invalid.", e.getMessage());
			e.printStackTrace();
		}
	}

	// Test011
	// Objective: Test to catch exception when surname is not entered.
	// Inputs: "Tom", "",1,1,1,1,1,1,1
	// Output: Catch exception firstName invalid.
	public void testUpdateCustomerDaysOfWeek002() {
		try {
			dao.updateCustomerDaysOfWeek("Tom", "", 1, 1, 1, 1, 1, 1, 1);
			fail("Should Not Get Here");
		} catch (exceptionHandler e) {
			assertSame("surname invalid.", e.getMessage());
			e.printStackTrace();
		}
	}

	// Test012
	// Objective: Test to catch exception when Monday is null.
	// Inputs: "Tom", "Dooley",null,1,1,1,1,1,1
	// Output: Catch exception firstName invalid.
	public void testUpdateCustomerDaysOfWeek003() {
		try {
			dao.updateCustomerDaysOfWeek("Tom", "Dooley", null, 1, 1, 1, 1, 1, 1);
			fail("Should Not Get Here");
		} catch (exceptionHandler e) {
			assertSame("invalid number.", e.getMessage());
			e.printStackTrace();
		}
	}

	// Test013
	// Objective: Test to catch exception when Tuesday is null.
	// Inputs: "Tom", "Dooley",1,null,1,1,1,1,1
	// Output: Catch exception firstName invalid.
	public void testUpdateCustomerDaysOfWeek004() {
		try {
			dao.updateCustomerDaysOfWeek("Tom", "Dooley", 1, null, 1, 1, 1, 1, 1);
			fail("Should Not Get Here");
		} catch (exceptionHandler e) {
			assertSame("invalid number.", e.getMessage());
			e.printStackTrace();
		}
	}

	// Test014
	// Objective: Test to catch exception when Wednesday is null.
	// Inputs: "Tom", "Dooley",1,1,null,1,1,1,1
	// Output: Catch exception firstName invalid.
	public void testUpdateCustomerDaysOfWeek005() {
		try {
			dao.updateCustomerDaysOfWeek("Tom", "Dooley", 1, 1, null, 1, 1, 1, 1);
			fail("Should Not Get Here");
		} catch (exceptionHandler e) {
			assertSame("invalid number.", e.getMessage());
			e.printStackTrace();
		}
	}

	// Test015
	// Objective: Test to catch exception when Thursday is null.
	// Inputs: "Tom", "Dooley",1,1,null,1,1,1,1
	// Output: Catch exception firstName invalid.
	public void testUpdateCustomerDaysOfWeek006() {
		try {
			dao.updateCustomerDaysOfWeek("Tom", "Dooley", 1, 1, 1, null, 1, 1, 1);
			fail("Should Not Get Here");
		} catch (exceptionHandler e) {
			assertSame("invalid number.", e.getMessage());
			e.printStackTrace();
		}
	}

	// Test016
	// Objective: Test to catch exception when Friday is null.
	// Inputs: "Tom", "Dooley",1,1,null,1,1,1,1
	// Output: Catch exception firstName invalid.
	public void testUpdateCustomerDaysOfWeek007() {
		try {
			dao.updateCustomerDaysOfWeek("Tom", "Dooley", 1, 1, 1, 1, null, 1, 1);
			fail("Should Not Get Here");
		} catch (exceptionHandler e) {
			assertSame("invalid number.", e.getMessage());
			e.printStackTrace();
		}
	}

	// Test017
	// Objective: Test to catch exception when Saturday is null.
	// Inputs: "Tom", "Dooley",1,1,null,1,1,1,1
	// Output: Catch exception firstName invalid.
	public void testUpdateCustomerDaysOfWeek008() {
		try {
			dao.updateCustomerDaysOfWeek("Tom", "Dooley", 1, 1, 1, 1, 1, null, 1);
			fail("Should Not Get Here");
		} catch (exceptionHandler e) {
			assertSame("invalid number.", e.getMessage());
			e.printStackTrace();
		}
	}

	// Test018
	// Objective: Test to catch exception when Saturday is null.
	// Inputs: "Tom", "Dooley",1,1,null,1,1,1,1
	// Output: Catch exception firstName invalid.
	public void testUpdateCustomerDaysOfWeek009() {
		try {
			dao.updateCustomerDaysOfWeek("Tom", "Dooley", 1, 1, 1, 1, 1, 1, null);
			fail("Should Not Get Here");
		} catch (exceptionHandler e) {
			assertSame("invalid number.", e.getMessage());
			e.printStackTrace();
		}
	}

	// Testing the customerOptInOut method
	// Test019
	// Objective: Test if True to succesfully opt a customer out of the
	// publication deliveries
	// Inputs:"true","Tom" "Dooley"
	// Output: true
	public void testCustomerOptInOut001() {
		try {
			assertEquals(true, dao.customerOptInOut(true, "Tom", "Dooley"));
		} catch (exceptionHandler | SQLException e) {
			e.printStackTrace();
		}
	}

	// Testing the customerOptInOut method
	// Test020
	// Objective: Test if True to succesfully opt a customer out of the
	// publication deliveries
	// Inputs:"false","Tom" "Dooley"
	// Output: false
	public void testCustomerOptInOut002() {
		try {
			assertEquals(false, dao.customerOptInOut(false, "Tom", "Dooley"));
		} catch (exceptionHandler | SQLException e) {
			e.printStackTrace();
		}
	}
	// ---------------------------------------------------------------------------------
	// DPL1 Delivery Person Login

	// Test 21
	// Objective: Test that type of user is a valid delivery person
	// Inputs: true, Tom, password
	// Output: true
	public void testDeliverPersonLogin001() {
		assertEquals(true, dao.deliveryPersonLogin("Tom", "password", 0));
	}

	// Test 22
	// Objective: Test that type of user is invalid when username is incorrect
	// Inputs: false, Tom, password,0
	// Output: false
	public void testDeliverPersonLogin002() {
		assertEquals(false, dao.deliveryPersonLogin("Ben", "password", 0));
	}

	// Test 23
	// Objective: Test that type of user is invalid when password is incorrect
	// Inputs: false, Tom, pass,0
	// Output: false
	public void testDeliverPersonLogin003() {
		assertEquals(false, dao.deliveryPersonLogin("Tom", "pass", 0));
	}

	// Test 22
	// Objective: Test that type of user is invalid with both username and
	// password correct
	// and type of user incorrect
	// Inputs: false, Tom, password, 1
	// Output: false
	public void testDeliverPersonLogin004() {
		assertEquals(false, dao.deliveryPersonLogin("Tom", "password", 1));
	}
}
