package testing;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import java.text.ParseException;

import production.User;
import junit.framework.Assert;
import junit.framework.TestCase;

public class UserTest extends TestCase {
	int customer;
	Date date;
	User user;
	boolean expected;
	
	/*
	 * Test Id: CompleteDelivery1
	 * Inputs: customer=0, date=2016-3-1
	 * Expected output=false
	 * Objective: Test with an invalid customer
	 */
	
	public void testCompleteDelivery1(){
		
		customer = 0;
		date = new Date();
		date.setYear(2016);
		date.setMonth(3);
		date.setDate(1);	
		
		AdminDAOMock mock = new AdminDAOMock();
		mock.setBoolToReturn(true);
		FactoryAdminDAO.instance.setAdminDAO(mock);
		
		user = new User();
		
		expected = false;
		
		Assert.assertEquals(expected, user.completeDelivery(customer, date));
	}
	
	/*
	 * Test Id: CompleteDelivery2
	 * Inputs: customer=0, date=null
	 * Expected output=false
	 * Objective: Test with an invalid customer
	 */
	
	public void testCompleteDelivery2(){
		
		customer = 1;
		date = null;	
		
		AdminDAOMock mock = new AdminDAOMock();
		mock.setBoolToReturn(true);
		FactoryAdminDAO.instance.setAdminDAO(mock);
		
		user = new User();
		
		expected = false;
		
		Assert.assertEquals(expected, user.completeDelivery(customer, date));
	}
	
	/*
	 * Test Id: CompleteDelivery3
	 * Inputs: customer=1, date=2016-3-3
	 * Expected output=false
	 * Objective: Correct customer and date but the delivery doesn't exist
	 */
	
	public void testCompleteDelivery3(){
		
		customer = 1;
		date = new Date();
		date.setYear(2016);
		date.setMonth(3);
		date.setDate(3);	
		
		AdminDAOMock mock = new AdminDAOMock();
		mock.setBoolToReturn(true);
		FactoryAdminDAO.instance.setAdminDAO(mock);
		
		user = new User();
		
		expected = false;
		
		Assert.assertEquals(expected, user.completeDelivery(customer, date));
	}
	
	/*
	 * Test Id: CompleteDelivery4
	 * Inputs: customer=0, date=2016-3-1
	 * Expected output=true
	 * Objective: Correct customer and date and the delivery exists
	 */
	
	public void testCompleteDelivery4(){
		
		customer = 0;
		date = new Date();
		date.setYear(2016);
		date.setMonth(3);
		date.setDate(3);	
		
		AdminDAOMock mock = new AdminDAOMock();
		mock.setBoolToReturn(true);
		FactoryAdminDAO.instance.setAdminDAO(mock);
		
		user = new User();
		
		expected = false;
		
		Assert.assertEquals(expected, user.completeDelivery(customer, date));
	}
	
	
	
	/*
	 * Test Id: ListDeliveries1
	 * Inputs: date=null
	 * Expected output=null
	 * Objective: Wrong date because it is null
	 */
	
	public void testListDeliveries1(){
		
		customer = 0;
		date = null;	
		
		
		user = new User();
		
		expected = false;
		
		Assert.assertEquals(null, user.listDeliveries(date));
	}
	
	/*
	 * Test Id: ListDeliveries2
	 * Inputs: date=2016-03-03
	 * Expected output=Empty arraylist
	 * Objective: Try with a date when there isn't any delivery
	 */
	
	
	
	public void testListDeliveries2(){
		
		customer = 0;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			date = formatter.parse("2016-03-03");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ArrayList<String> expected = new ArrayList<String>();
		
		user = new User();
		
		
		Assert.assertEquals(expected, user.listDeliveries(date));
	}
	
	/*
	 * Test Id: ListDeliveries3
	 * Inputs: date=2016-03-01
	 * Expected output=Array list with 2 deliveries
	 * Objective: Try with a date when there are some deliveries 
	 */
	
	public void testListDeliveries3(){
		
		customer = 0;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			date = formatter.parse("2016-03-01");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ArrayList<String> expected = new ArrayList<String>();
		expected.add("id=11 customer=1 newsagent=1");
		expected.add("id=12 customer=1 newsagent=3");
		
		user = new User();
		
		
		Assert.assertEquals(expected, user.listDeliveries(date));
	}
	


}
