import junit.framework.TestCase;


public class MainGUIcontentTest extends TestCase {

	// test1
	// test object valid name A---Z
	// input : name = "Daily Record " , price = "4" 
	// expected : exception
	public void testNameAZ() {
		try {
			MainGUIcontent.actionPerformed("Daily Record", "4");
		} catch (Exception e1) {
			assertSame("names AZ", e1.getMessage());
			e1.printStackTrace();
		}
	}	
	// test2
		// test object valid name Z---A
		// input : name = "Times" , price = "1" 
		// expected : exception
	public void testNameZA() {
		try {
			MainGUIcontent.actionPerformed("Times", "1");
		} catch (Exception e1) {
			assertSame("names ZA", e1.getMessage());
			e1.printStackTrace();
		}
	}
	// test3
			// test object valid price HIGH---LOW
			// input : name = "Daily Record", price= "4"
			// expected : exception
	public void testPriceHL() {
		try {
			MainGUIcontent.actionPerformed("Daily Record", "4");
		} catch (Exception e1) {
			assertSame("price HL", e1.getMessage());
			e1.printStackTrace();
		}
	}
	// test4
			// test object valid name LOW---HIGH
			// input : name = "Times" , price = "1" 
			// expected : exception
	public void testPriceLH() {
		try {
			MainGUIcontent.actionPerformed("Times", "1");
		} catch (Exception e1) {
			assertSame("price LH", e1.getMessage());
			e1.printStackTrace();
		}
	}
}
