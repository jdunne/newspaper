package testing;

public enum FactoryAdminDAO {
	instance;
	
	private AdminDAO adminDAO;
	
	public AdminDAO getAdminDAO(){
		
		if(adminDAO==null)
			return new AdminDAO();
		else return adminDAO;
		
	}
	
	public void setAdminDAO(AdminDAO adminDAO){
		this.adminDAO = adminDAO;
	}

}
