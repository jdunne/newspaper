package production;


public class Customer {

	private int id;
	private String firstName;
	private String surname; 
	private String phoneNo;
	private boolean status;
	private Address address;
	//changed this
	private String paymentType;
	
	public Customer(){ 
		
	}

	public Customer(int id, String firstName, String surname, String phoneNo, boolean status, String street, int number,
			int segment, String town,String paymentType) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.surname = surname;
		this.phoneNo = phoneNo;
		this.status = status;
		address = new Address(street, number, segment, town);
		//changed this
		this.paymentType=paymentType;
	}

	public Address getAdress() {
		return address;
	}

	public void setAdress(Address adress) {
		this.address = adress;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

}
