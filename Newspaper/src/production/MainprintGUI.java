import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class MainprintGUI extends JFrame implements ActionListener
	{
		private JMenuItem exitItem;

		public MainprintGUI()
		{
			
			super( "Print Delivery" ); 
			
			
			JMenuBar menuBar=new JMenuBar();
			JMenu fileMenu=new JMenu("File");
			exitItem =new JMenuItem("Exit");
	
			fileMenu.add(exitItem);
			menuBar.add(fileMenu );
			setJMenuBar(menuBar);
			
			
			exitItem.addActionListener(this);

			
			MainGUIcontent aWindowContent = new MainGUIcontent( "Print Delivery");
			
			getContentPane().add( aWindowContent );
			
			setSize( 900, 150 );
			setVisible( true );
		}
		
		
		public void actionPerformed(ActionEvent e)
		{
			if(e.getSource().equals(exitItem)){
				this.dispose();
			}
		}
		
		
		
	}