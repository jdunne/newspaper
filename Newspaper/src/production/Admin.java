package production;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import testing.AdminDAO;
import testing.FactoryAdminDAO;

import com.mysql.jdbc.ResultSetMetaData;

import exceptionHandling.exceptionHandler;


public class Admin extends User {
	java.sql.Connection con; 
    java.sql.Statement stat=null;
    ResultSet rs= null;
    java.sql.ResultSetMetaData rsmd=null;
    DAO dao;
    DAO DB=new DAO();
    
 
	private static PreparedStatement psmt = null;
    
    public boolean connect()throws Exception{
		 try
			{
				// Load the JConnector Driver
				Class.forName("com.mysql.jdbc.Driver");
				// Specify the DB Name
				String url="jdbc:mysql://localhost:3306/Newspapers";
				// Connect to DB using DB URL, Username and password
				con =  DriverManager.getConnection(url, "root", "admin");
				//Create a generic statement which is passed to the TestInternalFrame1
				stat =  con.createStatement();
				return true;
			}
			catch(Exception e)
			{
				throw new Exception("failed to connect");
			}
         
			 
	 }

	public Admin(int id, String username, String password) {
		super(id, username, password);
		// TODO Auto-generated constructor stub
	}
	
	

	public Admin() {
		super();
	}

	// Issue encountered with returning employee here, changed to boolean to let
	// us know if
	// Creating employee was successful, also need second password to confirm
	// DO we need a variable type in the base class?
	//Employee registered can be an admin or a regular emp
	public boolean addEmployee(String userName, String password1,
			String password2, Boolean admin) throws exceptionHandler,
			SQLException {

		boolean register = false;

		//Ensures username isn't null
		if(userName == null){
			throw new exceptionHandler(
					"Invalid username, cannot be null");

		}

		//Ensures passwords aren't null
		else if(password1 == null){
			throw new exceptionHandler(
					"Invalid password, cannot be null");
		}
		
		//Check to see if the admin boolean has been initialised 
		else if(admin == null){
			throw new exceptionHandler(
					"Invalid user, must specify they are an admin");
		}


		// Check if the username contains a number 
		boolean userNameNum = false;

		for (int i = 0; i < userName.length(); i++) {
			if (Character.isDigit(userName.charAt(i))) {
				userNameNum = true;
			}
		}

		// Checks if the password has a number
		boolean userPasswordNum = false;

		for (int i = 0; i < password1.length(); i++) {
			if (Character.isDigit(password1.charAt(i))) {
				userPasswordNum = true;
			}
		}

		// checks the password contains at least one letter
		boolean userPasswordChar = false;
		for (int i = 0; i < password1.length(); i++) {
			if (Character.isLetter(password1.charAt(i))) {
				userPasswordChar = true;
			}
		}

		// Check if the passwords match
		if (!password1.equals(password2)) {
			throw new exceptionHandler(
					"Invalid password, first password must match second");
		}

		// Raises exception if there is a number in the username (Test 2)
		else if (userNameNum) {
			throw new exceptionHandler(
					"Invalid Username, must not contain numbers");
		}

		// Checks the length of the username (Test 3)
		else if (userName.length() < 2) {
			throw new exceptionHandler(
					"Invalid Username, must have at least 2 letters");
		}

		// Checks the password has a number
		else if (!userPasswordNum) {
			throw new exceptionHandler(
					"Invalid password, must contain a number");
		}

		// Checks the password has a length of 8 or more
		else if (password1.length() < 8) {
			throw new exceptionHandler(
					"Invalid password, must have a length of at least 8");
		}
  
		// checks if the password contains a letter
		else if (!userPasswordChar) {
			throw new exceptionHandler(
					"Invalid password, passwords must have letters and numbers");
		}


		else {
			register = true;
			DAO dao = new DAO();
			User user = new User( userName, password1);
			dao.registerEmployee(user, admin);
		}

		return register;

	}	
	
	//This will check if there are any accounts which have not 
	//Paid and deactivates them. There are checks to ensure only 
	//accounts which have not paid are deactivated
	public boolean deactivateUnpaidAccount() throws exceptionHandler, SQLException{
		
		boolean deactivate = true;
		
		DAO dao = new DAO();
		deactivate = dao.deactivateAccounts();
		
		if(!deactivate)
			throw new exceptionHandler("No customers to update!");
		
		return deactivate;
		
	}
	
	public boolean removeEmployee(int id){
		
		return false;
		
	}
	
	public boolean assignRoute(int employee, int publication, int customer, java.util.Date date, int quantity) {
		
		if(employee<=0 || publication<=0 || customer<=0 || date==null || quantity<=0)
			return false;
		
		
		AdminDAO dao = FactoryAdminDAO.instance.getAdminDAO();
		
		return dao.assignRoute(employee, publication, customer, date, quantity);

		
	}

	
	public boolean alterCustomer(boolean status, Customer customer){
		
		return false;
		
	}
	
	// Gary User Story ADR6 Register Customer
		// -----------------------------------------------------------------------------------//
		public boolean addCustomer(String firstName, String surname, String street, int number, int segment, String town,
				String phoneNo) throws exceptionHandler {
			boolean result = false;
			if (firstName.equals(""))
				throw new exceptionHandler("First Name Missing.");
			else if (surname.equals(""))
				throw new exceptionHandler("Surname Missing.");
			else if (street.equals(""))
				throw new exceptionHandler("Street Missing.");
			else if (number == 0)
				throw new exceptionHandler("Number Missing.");
			else if (segment == 0)
				throw new exceptionHandler("Value is Invalid.");
			else if (town.equals(""))
				throw new exceptionHandler("Town Missing.");
			else if (phoneNo.equals(""))
				throw new exceptionHandler("Phone Number Missing.");
			else if (segment < 1 || segment > 4) 
				throw new exceptionHandler("Value is Invalid.");
			else
				dao.registerCustomer(firstName, surname, street, number, segment, town, phoneNo);
				result = true;
			
			return result;
		}

		public boolean addPaymentDetails(String paymentDetails) throws exceptionHandler {
			boolean result = false;
			if (paymentDetails.equals("Cash"))
				result = true;
			else if (paymentDetails.equals("Cheque"))
				result = true;
			else if (paymentDetails != "Cash" || paymentDetails != "Cheque")
				throw new exceptionHandler("Wrong Payment Type");
			return result;
			
		}
	
	public boolean removeCustomer(int id){
		
		try {
			connect();
		} catch (Exception e) {

			e.printStackTrace();
		}
		try {
			int res = DB.removeCustomer(id);
			System.out.println(res + " line have been modified ");

			if (res == 1) {
				return true;

			} else
				return false;
		}

		catch (SQLException sqle) {
			System.err.println("Error with searching:\n" + sqle.toString());
			return false;
		}

		
	}
	
	public static boolean addPublication(String name, float price) throws Exception {
		Connection connection = DAO.connectA();
		System.out.println("connection ok");
		String SQL = "insert into publications (name, price) values(?, ?)";
		psmt = connection.prepareStatement(SQL);
		psmt.setString(1, name);
		psmt.setFloat(2, price);
		System.out.println("testing param");

		// check name for only letters
		Pattern stringPattern = Pattern.compile("^[a-zA-Z\\s]*$");
		Matcher m = stringPattern.matcher(name);

		// check price for only numbers
		String checkPrice = String.valueOf(price);
		Pattern floatPattern = Pattern.compile("^[0-9]*\\.[0-9]*$");
		Matcher f = floatPattern.matcher(checkPrice);

		if (name != "") {
			if (m.matches()) {
				if (name.length() > 3) {
					if (price > 0.0f) {
						if (f.matches()) {
							psmt.executeUpdate();
							System.out.println("db updated");
							return true;
						} else
							return false;
					} else
						return false;
				} else
					return false;
			} else
				return false;
		} else
			return false;

	}
	
	public static int removePublication(String name) throws Exception {
		Connection connection = DAO.connectA();
		System.out.println("connection ok");
		String SQL = ("delete from publications where name = ?");
		psmt = connection.prepareStatement(SQL);
		psmt.setString(1, name);

		// check name for only letters
		Pattern stringPattern = Pattern.compile("^[a-zA-Z\\s]*$");
		Matcher m = stringPattern.matcher(name);
		int i = 2;
		if (name != "") {
			if (m.matches()) {
				i = psmt.executeUpdate();
				System.out.println("db updated");
				return i;
			} else
				return i;
		} else
			return i;

	}
	
	public static int alterCustomerDeliveryDay(int cID ,int monday, int tuesday, int wednesday, int thursday,
			int friday, int saturday, int sunday) throws Exception {

		Connection connection = DAO.connectA();
		System.out.println("connection ok");
		String SQL="UPDATE DaysWeek  SET monday=?, tuesday=?, wednesday=?, thursday=?,friday=?, saturday=?, sunday=? WHERE cID=?";
		//String SQL = "insert into DaysWeek (monday, tuesday, wednesday, thursday,friday, saturday, sunday ) values(?,?,?,?,?,?,?)";
		psmt = connection.prepareStatement(SQL);
		psmt.setInt(1, monday);
		psmt.setInt(2, tuesday);
		psmt.setInt(3, wednesday);
		psmt.setInt(4, thursday);
		psmt.setInt(5, friday);
		psmt.setInt(6, saturday);
		psmt.setInt(7, sunday);
		psmt.setInt(8, cID);
		int i=2;
		 i = psmt.executeUpdate();
		if(i==1){
			System.out.println("db updated");
			return i;
		}else if(i==0){
			System.out.println("No Change");
			return i;
		}
		return i;

	}
	
	
	//This method is to change the publication price ��
	//and the change will be showed in database
	public boolean changePublicationPrice(String name, float price){
		try{
			connect();
		}catch (Exception e){
		     e.printStackTrace();
		}
		 try{
			  if(price>0){
			  String updateAdmin="UPDATE Publication SET price='"+price+"' WHERE name='"+name+"';";
			  stat.executeUpdate(updateAdmin);  
			         return true; }
			   else  return false;
		   }
		 catch (SQLException sqle){
			System.err.println("Error with insert:\n"+sqle.toString());
			return false;
			//if(sqle.toString().endsWith(name)) System.out.println("please reset the name");
			} 
		}
			
		
	public boolean changeDelivery(float cost){
		
		try {
			connect();
		} catch (Exception e) {

			e.printStackTrace();
		}
		try {
			int res = 0;
			if (cost < 10 && cost > 0) {
			
				res = DB.changeDelivery(cost);
				System.out.println(res + " line have been modified ");
			} else
				System.out.println("invalid input,please try again");
			if (res == 1) {
				System.out.println("change Delivery Cost (per unit) Operation successful");
				return true;

			} else
				return false;

		}

		catch (SQLException sqle) {
			System.err.println("Error with searching:\n" + sqle.toString());
			return false;
		}
		
	}
	
	
	// write method that could calculate the price of each publication
		static double cost=0;
		public static void main(String args[]) 
	    {
		        int quantity;
		        String quantityString;

		        quantityString = JOptionPane.showInputDialog("Enter quantity ordered: ");
		        quantity = Integer.parseInt(quantityString);

		        if (quantity == 1)
		        {
		            cost = (.5);
		        }
		        else if(quantity ==2)
		        {
		            cost = (1);
		        }
		        else  if(quantity >= 3)	
		        {
		            cost = (1.5);
		        }	        
		        changeDeliveryCost( quantity,cost);
		        System.exit(0);

		    } // End of main() method.

		    // Write changeDeliveryCost method here.
		
		        public static boolean changeDeliveryCost(int quantity,double cost) 
		        {
		        	boolean changeDeliveryCost = true;
		          System.out.println(" quantity:  " + quantity + "  cost:  " + cost);     
				return changeDeliveryCost;
		        }

	
	public boolean changeMaxDeliveryCost(float cost){
		
		try {
			connect();
		} catch (Exception e) {

			e.printStackTrace();
		}

		try {
			int res = 0;
			if (cost < 10 && cost > 0) {

				
				res=DB.changeMaxDeliveryCost(cost);
				System.out.println(res + " line have been modified ");
			} else
				System.out.println("invalid input,please try again");

			if (res == 1) {
				System.out.println("Change Max Delivery Cost Operation successful");

				return true;

			} else
				return false;

		}

		catch (SQLException sqle) {
			System.err.println("Error with searching:\n" + sqle.toString());
			return false;
		}
		
	}
	
	
	//This method is to list the most popular publication
		public List<Publication> searchTheMostPopular(){ 
			
			try {
				connect();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
			List<Publication> PublicationList = new ArrayList<Publication>();
		      try {
		        String selectPublication="SELECT * FROM Publication order by price DESC";
		        rs=stat.executeQuery(selectPublication);
		        
		    	   while(rs.next()){
		    	   Publication p = new Publication( rs.getString("name"), rs.getFloat("price"));
		    	   PublicationList.add(p);}
		      } 
		      catch (SQLException e) {
		         e.printStackTrace();
		      }
		 return PublicationList;
	}
		
		
		
		//This is a most popular list function
		public boolean mostpopularList(){
			boolean l=false;
			List<Publication> publication=searchTheMostPopular();
				if(publication.size()>0){
					for(int i=0;i<publication.size();i++){
						Publication publications=(Publication)publication.get(i);
						System.out.println(publications.getName()+""+publications.getPrice());	
					}
					l=true;
				}
					return l;		
		}
		

		
		//This method is to print summary by selected date
		public String PrintSummary(String date) {
		try{
		connect();
		}catch (Exception e){
		     e.printStackTrace();
		}
			try {
			String toExcute = ("SELECT * from summary where date = " + date + ";");
			rs = stat.executeQuery(toExcute);
			
				while(rs.next()) {
				 
					String summary = rs.getInt("id") + "," + rs.getString("firstName") + ","
							+ rs.getString("surname") + ","
							+ date+","
							+ rs.getInt("status");
					System.out.println(summary);
							
					return summary;
}
} catch (Exception e) {
		e.printStackTrace();
	}
		return null;
		}


		
public boolean PrintSummaryByDate(String date){
	Admin p= new Admin();
	
	 
	if(!date.isEmpty()){ 
		System.out.println( p.PrintSummary(date));
	 return true;
	}
	else
		 
		 return false;
	
}
//Cian user story------------------------
public void generateSummary()
{
	DAO dao = new DAO();
	List<String> summaryArray = dao.getSummary();

	String printSummary = "";
	for (String s : summaryArray) {
		printSummary += s + "\n";
	}
	System.out.println(printSummary);
}


}


				
