import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class MainprintGUI2 extends JFrame implements ActionListener
	{
		private JMenuItem exitItem;

		public MainprintGUI2()
		{
			
			super( "Print Delivery Routes" ); 
			
			
			JMenuBar menuBar=new JMenuBar();
			JMenu fileMenu=new JMenu("File");
			exitItem =new JMenuItem("Exit");
	
			fileMenu.add(exitItem);
			menuBar.add(fileMenu );
			setJMenuBar(menuBar);	
			exitItem.addActionListener(this);

			
			MainGUIcontent2 aWindowContent = new MainGUIcontent2( "Print Delivery Routes");
			
			getContentPane().add( aWindowContent );
			
			setSize( 400, 150 );
			setVisible( true );
		}
		
		
		public void actionPerformed(ActionEvent e)
		{
			if(e.getSource().equals(exitItem)){
				this.dispose();
			}
		}
		
		
		
	}