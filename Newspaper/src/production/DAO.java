package production;

import java.sql.Connection;
import java.util.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import exceptionHandling.exceptionHandler;
import exceptionHandling.EmployeeIDExceptionHandler;

public class DAO {

	// We're probably going to be connecting in various ways
	// So a generic connect method is added.
	private Connection connection = null;
	private Statement statement = null;
	private Statement statementAddress = null;
	private Statement statementCustomer = null;
	private Statement statementSetDaysOfWeek = null;
	private PreparedStatement prepareStatementSetDaysOfWeek = null;

	private ResultSet resultSet = null;
	private ResultSet resultSetID = null;
	private ResultSet resultSetCustomer = null;
	private ResultSet resultSetAddress = null;
	private ResultSet resultSetDaysOfWeek = null;
	private ResultSet resultSetDeliverPersonLogin;
	private boolean result;
	private int daysWeekId;
	String cmd = null;
	private Connection con;
	private Statement stmt;
	private ResultSet rs;
	
	Date date;
	int ID;

	public DAO() {

	}

	public DAO(Connection connection, Statement statement, ResultSet resultSetID) {
		super();
		this.connection = connection;
		this.statement = statement;
		this.resultSetID = resultSetID;
	}

	public void connect() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3307/newspaper", "root", "gazz");
			statement = connection.createStatement();
			statementCustomer = connection.createStatement();
			statementAddress = connection.createStatement();
			statementSetDaysOfWeek = connection.createStatement();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean registerEmployee(User user, boolean admin) throws SQLException, exceptionHandler {

		boolean register = true;
		User u = user;

		// This is the variable which will determine if the user
		// is created as an admin or not
		int adminValue = 0;

		// Adds the user as an admin
		if (admin)
			adminValue = 1;
		// Adds user as standard user
		else
			adminValue = 0;

		String insert = "INSERT INTO User (username, password, type) VALUES( '" + u.getUsername() + "','"
				+ u.getPassword() + "','" + adminValue + "');";

		int count = stmt.executeUpdate(insert);
		stmt.close();
		con.close();

		return register;

	}

	// This method checks to see if an invalid user which has been attempted to
	// be input
	// during the admin tests have not been added to the database. Admin task
	public boolean validateEmployeesRegistered(User user) throws SQLException, exceptionHandler {

		boolean found = false;

		String employeeQueery = "Select * from User;";

		rs = stmt.executeQuery(employeeQueery);

		// While loop which iterates iver the resultset to see if this user is
		// in the database
		while (rs.next()) {
			if ((user.getUsername().equals(rs.getString("username")))
					&& user.getPassword().equals(rs.getString("password"))) {
				found = true;
			}
		}

		// An exception is thrown if the user exists in the database as they
		// invaild
		if (found)
			throw new exceptionHandler("Exception, this user is invalid and should not feature in the database");

		return found;
	}

	public boolean deactivateAccounts() throws SQLException {

		boolean deactivate = true;
		String update = "Update customer Set status = 0 where paid = 0 And status = 1;";
		int updateStatus = stmt.executeUpdate(update);

		if (updateStatus == 0) {
			deactivate = false;
		}

		return deactivate;
	}

	// ------------------------------------------------------------------------------------------------------------------------------
	public boolean registerCustomer(String firstName, String surname, String street, int number, int segment,
			String town, String phoneNo) {
		connect();
		int addressId = 0;
		int daysWeekId = 0;
		result = false;
		try {
			// statement to add and address
			statement.executeUpdate("insert into address values(" + null + ",'" + street + "'," + number + "," + segment
					+ ",'" + town + "');");
			// this is to get the last id that was added to the address table
			// and the id returned will represent the address for the customer
			// table
			resultSetID = statement.executeQuery("select max(id) from address;");
			if (resultSetID.next()) {
				addressId = resultSetID.getInt(1);
			}
			// This will set all delivery days to a default of zero which
			// represent false or not set in the database
			statement.executeUpdate("insert into daysweek values(null,0,0,0,0,0,0,0);");
			resultSetID = statement.executeQuery("select max(id) from daysweek;");
			if (resultSetID.next()) {
				daysWeekId = resultSetID.getInt(1);
			}
			// this will add the new customer
			statement.executeUpdate("insert into customer values(" + null + "," + addressId + ",'" + firstName + "','"
					+ surname + "','" + phoneNo + "'," + 0 + "," + daysWeekId + ");");
			result = true;
		} catch (SQLException e) {
			e.printStackTrace();

		}
		return result;

	}

	public boolean registerValidation(String firstName, String surname, String street, int number, int segment,
			String town, String phoneNo) throws exceptionHandler, SQLException {
		connect();
		result = false;
		// Two resukt set are needed here. Checks must be done against both
		// database tables at he same time
		resultSetCustomer = statementAddress.executeQuery("select * from customer;");
		resultSetAddress = statementCustomer.executeQuery("select * from address;");
		while (resultSetCustomer.next() && resultSetAddress.next()) {
			if (resultSetCustomer.getString("first_name").equals(firstName)
					&& resultSetCustomer.getString("surname").equals(surname)
					&& resultSetCustomer.getString("phoneNumber").equals(phoneNo)
					&& resultSetAddress.getString("street").equals(street)
					&& resultSetAddress.getInt("number") == number && resultSetAddress.getInt("segment") == segment
					&& resultSetAddress.getString("town").equals(town))
				result = true;
			else
				result = false;
		}
		return result;
	}

	// setting method parameters as Integers and not int so they can be check if
	// they are equal to null.
	public void updateCustomerDaysOfWeek(String firstName, String surname, Integer monday, Integer tuesday,
			Integer wednesday, Integer thursday, Integer friday, Integer saturday, Integer sunday)
					throws exceptionHandler {
		connect();

		daysWeekId = 0;

		// Check Variable Sent into the method and throw new exceptions if test
		// variables are incorrect.
		if (firstName.equals(""))
			throw new exceptionHandler("firstName invalid.");
		if (surname.equals(""))
			throw new exceptionHandler("surname invalid.");
		if (monday == null)
			throw new exceptionHandler("invalid number.");
		if (tuesday == null)
			throw new exceptionHandler("invalid number.");
		if (wednesday == null)
			throw new exceptionHandler("invalid number.");
		if (thursday == null)
			throw new exceptionHandler("invalid number.");
		if (friday == null)
			throw new exceptionHandler("invalid number.");
		if (saturday == null)
			throw new exceptionHandler("invalid number.");
		if (sunday == null)
			throw new exceptionHandler("invalid number.");

		try {
			resultSetDaysOfWeek = statementSetDaysOfWeek.executeQuery(
					"select * from customer where first_name='" + firstName + "' and surname='" + surname + "';");
			while (resultSetDaysOfWeek.next()) {
				daysWeekId = resultSetDaysOfWeek.getInt("days");
			}
			prepareStatementSetDaysOfWeek = connection.prepareStatement(
					"update daysweek set monday = ?,tuesday=?,wednesday=?,thursday=?,friday=?,saturday=?,sunday=? where id=?;");
			prepareStatementSetDaysOfWeek.setInt(1, monday);
			prepareStatementSetDaysOfWeek.setInt(2, tuesday);
			prepareStatementSetDaysOfWeek.setInt(3, wednesday);
			prepareStatementSetDaysOfWeek.setInt(4, thursday);
			prepareStatementSetDaysOfWeek.setInt(5, friday);
			prepareStatementSetDaysOfWeek.setInt(6, saturday);
			prepareStatementSetDaysOfWeek.setInt(7, sunday);
			prepareStatementSetDaysOfWeek.setInt(8, daysWeekId);
			prepareStatementSetDaysOfWeek.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public boolean customerOptInOut(boolean optInOut, String firstName, String surname)
			throws exceptionHandler, SQLException {
		connect();
		daysWeekId = 0;
		if (optInOut == true) {
			// Reset Values in the data base for each day of week
			try {
				resultSetDaysOfWeek = statementSetDaysOfWeek.executeQuery(
						"select * from customer where first_name='" + firstName + "' and surname='" + surname + "';");
				while (resultSetDaysOfWeek.next()) {
					daysWeekId = resultSetDaysOfWeek.getInt("days");
				}
				prepareStatementSetDaysOfWeek = connection.prepareStatement(
						"update daysweek set monday = 0,tuesday=0,wednesday=0,thursday=0,friday=0,saturday=0,sunday=0 where id=?;");

				prepareStatementSetDaysOfWeek.setInt(1, daysWeekId);
				prepareStatementSetDaysOfWeek.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			}

		} else
			optInOut = false;
		return optInOut;

	}

	public boolean deliveryPersonLogin(String username, String password, int type) {
		connect();
		result = false;
		try {
			resultSet = statement.executeQuery("select * from user;");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			while (resultSet.next()) {
				int userType = resultSet.getInt("type");
				System.out.println(userType);
				if (type == userType && username.equals(resultSet.getString("username"))
						&& password.equals(resultSet.getString("password")))
					result = true;
				else {
					result = false;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static Connection connectA() throws Exception {

		String url = "jdbc:mysql://localhost:3306/Newspaper";
		String user = "root";
		String password = "admin";

		Class.forName("com.mysql.jdbc.Driver");
		Connection conA = DriverManager.getConnection(url, user, password);
		return conA;
	}

	// ------------------------------------------------------------------------------------------------------------------------------

	public List<String> getDeliveries(int id) throws EmployeeIDExceptionHandler {
		List<String> results = new ArrayList<String>();
		if (id > 0) {
			String testDate = "2016-03-14"; // for testing only change to today
											// for todays date
			connect();
			date = new Date();
			ID = id;
			java.sql.Date sqlDate = new java.sql.Date(date.getTime());

			try {
				String getDataSQL = "SELECT DISTINCT CONCAT(customer.first_name, ' ', customer.surname) As fullName, address.street, address.number, address.segment, quantity.amount, publication.name "
						+ "FROM address, quantity, publication, customer "
						+ "WHERE address.id IN (SELECT customer.address " + "FROM customer "
						+ "WHERE customer.id in (SELECT customer " + "FROM delivery " + "WHERE user = " + id
						+ " AND date = '" + testDate + "')) " + "AND customer.status = 1 "
						+ "AND customer.id IN (SELECT customer " + "FROM delivery " + "WHERE user = " + id
						+ " AND date = '" + testDate + "') " + "AND customer.address = address.id "
						+ "AND publication.id IN(SELECT publication " + "FROM quantity "
						+ "WHERE id IN(SELECT delivery.id " + "FROM delivery "
						+ "INNER JOIN customer ON customer.id = delivery.customer " + "WHERE delivery.user = " + id
						+ " AND delivery.date = '" + testDate + "' AND customer.status = 1)) "
						+ "AND quantity.id IN(SELECT delivery.id " + "FROM delivery "
						+ "INNER JOIN customer ON customer.id = delivery.customer " + "WHERE delivery.user = " + id
						+ " AND delivery.date = '" + testDate + "' AND customer.status = 1) "
						+ "AND publication.id = quantity.publication "
						+ "AND quantity.delivery = (SELECT id FROM delivery WHERE delivery.customer = customer.id) "
						+ "ORDER BY segment;";
				ResultSet resultSet = statement.executeQuery(getDataSQL);

				while (resultSet.next()) {
					String fullName = resultSet.getString("fullName");
					String street = resultSet.getString("street");
					String number = resultSet.getString("number");
					String segment = resultSet.getString("segment");
					String amount = resultSet.getString("amount");
					String name = resultSet.getString("name");

					String delivery = fullName + " || " + street + " " + number + " " + segment + " || " + name + " "
							+ amount;
					results.add(delivery);
				}

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			throw new EmployeeIDExceptionHandler("Invalid employee id");
		}

		return results;
	}

	public List<String> getSummary() {
		String testDate = "2016-03-14"; // for testing
		date = new Date();
		java.sql.Date sqlDate = new java.sql.Date(date.getTime());

		List<String> summaryArray = new ArrayList<String>();
		try {
			connect();
			String getSummary = "SELECT CONCAT(customer.first_name, ' ', customer.surname) AS fullName, publication.name, quantity.amount, delivery.date "
					+ "FROM customer, delivery, publication, quantity "
					+ "WHERE customer.id IN (SELECT customer FROM delivery WHERE completed = 1) "
					+ "AND delivery.completed = 1 " + "AND quantity.id IN(SELECT delivery.id " + "FROM delivery "
					+ "INNER JOIN customer ON customer.id = delivery.customer  " + "WHERE delivery.completed = 1)  "
					+ "AND publication.id = quantity.publication "
					+ "AND quantity.delivery IN (SELECT id FROM delivery WHERE delivery.customer = customer.id) "
					+ "AND quantity.delivery = delivery.id " + "AND delivery.date = '" + testDate + "';";

			ResultSet resultSet = statement.executeQuery(getSummary);

			while (resultSet.next()) {
				String fullName = resultSet.getString("fullName");
				String name = resultSet.getString("name");
				String amount = resultSet.getString("amount");
				String date = resultSet.getString("date");

				String delivery = fullName + " " + name + " " + amount + " " + date;
				summaryArray.add(delivery);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return summaryArray;

	}
	//*************************qin*************************//
	public int removeCustomer(int id) throws SQLException{
		int res=stmt.executeUpdate("DELETE FROM customer WHERE id =  '" + id + "'");
		return res;
	}
	public int changeMaxDeliveryCost(float cost) throws SQLException{
		int res = stmt.executeUpdate("update config  SET max_publications_charged =  '" + cost + "'");
		return res;
	}
	public int changeDelivery(float cost) throws SQLException{
		int res = stmt.executeUpdate("update config  SET charge_per_unit =  '" + cost + "'");
		return res;
	}
	
	
}