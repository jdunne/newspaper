 import java.awt.*;
import java.awt.event.*;
import java.io.FileWriter;
import java.io.PrintWriter;
import javax.swing.*;
import javax.swing.border.*;


import java.sql.*;
import java.text.ParseException;

import java.text.SimpleDateFormat;

import java.util.Date;

import java.util.Locale;


@SuppressWarnings("serial")
public class MainGUIcontent extends JInternalFrame implements ActionListener {
	String cmd = null;

	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;

	private Container content;

	private static QueryTableModel TableModel = new QueryTableModel();

	private JTable TableofDBContents = new JTable(TableModel);

	private JButton nameAZ = new JButton("nameA-Z");
	private JButton nameZA = new JButton("nameZ-A");
	private JButton priceLH = new JButton("priceH-L");
	private JButton priceHL = new JButton("priceL_H");

	private JPanel p1 = new JPanel();
	public MainGUIcontent(String aTitle) {

		super(aTitle, false, false, false, false);
		setEnabled(true);

		initiate_db_conn();
		
		content = getContentPane();
		content.setLayout(new BorderLayout());
		content.setBackground(Color.GRAY);

		nameAZ.setSize(100, 30);
		nameZA.setSize(100, 30);
		priceLH.setSize(100, 30);
		priceHL.setSize(100, 30);

		nameAZ.addActionListener(this);
		nameZA.addActionListener(this);
		priceLH.addActionListener(this);
		priceHL.addActionListener(this);

		content.add(p1);
		p1.add(nameAZ);
		p1.add(nameZA);
		p1.add(priceLH);
		p1.add(priceHL);

		
		setSize(900, 650);
		setVisible(true);

		TableModel.refreshFromDB(stmt);
	}

	public MainGUIcontent() {
		// TODO Auto-generated constructor stub
	}

	//database connectivity
	public void initiate_db_conn() {
		try {

			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/Newspapers";
			con = DriverManager.getConnection(url, "root", "");
			stmt = con.createStatement();
		} catch (Exception e) {
			System.out.println("Error: Failed to connect to database\n" + e.getMessage());
		}
	}

	public void actionPerformed(ActionEvent e) {
		Object target = e.getSource();

		if (target == this.nameAZ) {

			cmd = "select name,price from Publication order by name ASC;";

			try {
				rs = stmt.executeQuery(cmd);
				writeToFile(rs);
				throw new exceptionHandler("names AZ");
			} catch (Exception e1) {
				e1.printStackTrace();
			}

		}
		if (target == this.nameZA) {

			cmd = "select name,price from Publication order by name DESC;";

			try {
				rs = stmt.executeQuery(cmd);
				writeToFile(rs);
				throw new exceptionHandler("names ZA");
			} catch (Exception e1) {
				e1.printStackTrace();
			}

		}
		if (target == this.priceHL) {

			cmd = "select name,price from Publication order by price ASC;";

			try {
				rs = stmt.executeQuery(cmd);
				writeToFile(rs);
				throw new exceptionHandler("price HL");
			} catch (Exception e1) {
				e1.printStackTrace();
			}

		}
		if (target == this.priceLH) {

			cmd = "select name,price from Publication order by price DESC;";

			try {
				rs = stmt.executeQuery(cmd);
				writeToFile(rs);
				throw new exceptionHandler("price LH");
			} catch (Exception e1) {
				e1.printStackTrace();
			}

		}
		
		
	}

	///////////////////////////////////////////////////////////////////////////

	//file writer method
	private void writeToFile(ResultSet rs) {
		try {
			System.out.println("In writeToFile");
			FileWriter outputFile = new FileWriter("src/Summary.csv");
			PrintWriter printWriter = new PrintWriter(outputFile);
			ResultSetMetaData rsmd = rs.getMetaData();
			int numColumns = rsmd.getColumnCount();

			for (int i = 0; i < numColumns; i++) {
				printWriter.print(rsmd.getColumnLabel(i + 1) + ",");
			}
			printWriter.print("\n");
			while (rs.next()) {
				for (int i = 0; i < numColumns; i++) {
					printWriter.print(rs.getString(i + 1) + ",");
				}
				printWriter.print("\n");
				printWriter.flush();
			}
			printWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Object actionPerformed(String name, String string) {
		MainGUIcontent db = new MainGUIcontent();
		db.initiate_db_conn();
		
		return null;
	}
	public static void main( String args[] )
	{
		 new MainprintGUI();
	}

}
