package production;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import exceptionHandling.EmployeeIDExceptionHandler;
import testing.UserDAO;


public class User {
	
	
	private int id;
	private String username, password;
	
	public User(int id, String username, String password){
		this.id = id;
		this.username = username;
		this.password = password;
	}
	
	
	public User(String username, String password){
		this.id = id;
		this.username = username;
		this.password = password;
	}
	
	public User() {
		// TODO Auto-generated constructor stub
	}

	public boolean login(){
		
		return false;
		
	}
	
	public void printDeliveryRoute(int id) {

		try {
			DAO dao = new DAO();
			List<String> deliveryArray = dao.getDeliveries(id);

			String printDeliveries = "";
			for (String s : deliveryArray) {
				printDeliveries += s + "\n";
			}
			System.out.println(printDeliveries);
		} catch (EmployeeIDExceptionHandler e) {
			System.out.println("Invalid employee id");
		}


}
	
public boolean completeDelivery(int customer, Date date){
		
		UserDAO dao = new UserDAO();
		
		return dao.completeDelivery(customer, date);
		
		
	}

public ArrayList<String> listDeliveries(Date date){
	
	if(date==null)
		return null;
	
	UserDAO dao = new UserDAO();
	
	return dao.listDeliveries(date);
	
}



	public int getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	
	

}
