package production;

public class Address {
	private String street;
	private int number;
	private int segment;
	private String town;

	public Address(String street, int number, int segment, String town) {
		this.street = street;
		this.number = number;
		this.segment = segment;
		this.town = town;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getSegment() {
		return segment;
	}

	public void setSegment(int segment) {
		this.segment = segment;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

}
