 import java.awt.*;
import java.awt.event.*;
import java.io.FileWriter;
import java.io.PrintWriter;
import javax.swing.*;
import javax.swing.border.*;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.text.SimpleDateFormat;

import java.util.Date;

import java.util.Locale;


@SuppressWarnings("serial")
public class MainGUIcontent2 extends JInternalFrame implements ActionListener {
	String cmd = null;

	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;

	private Container content;

	private static QueryTableModel2 TableModel = new QueryTableModel2();

	private JTable TableofDBContents = new JTable(TableModel);


	private JButton DeliveryRoutes = new JButton("PrintDeliveryRoutes");

	private JPanel p1 = new JPanel();
	public MainGUIcontent2(String aTitle) {

		super(aTitle, false, false, false, false);
		setEnabled(true);

		initiate_db_conn();
		
		content = getContentPane();
		content.setLayout(new BorderLayout());
		content.setBackground(Color.GRAY);

		
		DeliveryRoutes.setSize(100, 30);
		DeliveryRoutes.addActionListener(this);
		content.add(p1);
		
		p1.add(DeliveryRoutes);

		setSize(900, 650);
		setVisible(true);

		TableModel.refreshFromDB(stmt);
	}

	public MainGUIcontent2() {
		// TODO Auto-generated constructor stub
	}

	//database connectivity
	public void initiate_db_conn() {
		try {

			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/Newspapers";
			con = DriverManager.getConnection(url, "root", "");
			stmt = con.createStatement();
		} catch (Exception e) {
			System.out.println("Error: Failed to connect to database\n" + e.getMessage());
		}
	}
	
	public void actionPerformed(ActionEvent e) {
		Object target = e.getSource();

		
		if (target == this.DeliveryRoutes) {
cmd = "select  Customer.first_name,Customer.surname,Customer.address,Customer.status,Customer.days,Publication.name from Customer,Publication where status is true;";
			try {
				rs = stmt.executeQuery(cmd);
				writeToFile(rs);
				throw new ExceptionHandler("status = true");
			} catch (Exception e1) {
				e1.printStackTrace();
			}

		}
		
		
	}

	///////////////////////////////////////////////////////////////////////////

	//file writer method
	private void writeToFile(ResultSet rs) {
		try {
			System.out.println("In writeToFile");
			// Print the current date 
			System.out.println(new SimpleDateFormat("dd-MM-YYYY").format(new Date()));
			FileWriter outputFile = new FileWriter("src/PrintDeliveryRoutes.csv");
			PrintWriter printWriter = new PrintWriter(outputFile);
			ResultSetMetaData rsmd = rs.getMetaData();
			int numColumns = rsmd.getColumnCount();

			for (int i = 0; i < numColumns; i++) {
				printWriter.print(rsmd.getColumnLabel(i + 1) + ",");
			}
			printWriter.print("\n");
			while (rs.next()) {
				for (int i = 0; i < numColumns; i++) {
					printWriter.print(rs.getString(i + 1) + ",");
				}
				printWriter.print("\n");
				printWriter.flush();
			}
			printWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Object actionPerformed(String string, String string2, String string3, String string4, String string5, String string6) {
		MainGUIcontent2 db = new MainGUIcontent2();
		db.initiate_db_conn();
		return null;
	}

	public static void main( String args[] )
	{
		 new MainprintGUI2();
	}
	
}
