package production;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.mysql.jdbc.Connection;

import exceptionHandling.exceptionHandler;

public class GUI extends JFrame implements ActionListener {

	private JPanel contentPane;

	private JTextField firstNameTF;
	private JTextField surnameTF;

	private JCheckBox mondayCB;
	private JCheckBox tuesdayCB;
	private JCheckBox wednesdayCB;
	private JCheckBox thursdayCB;
	private JCheckBox fridayCB;
	private JCheckBox saturdayCB;
	private JCheckBox sundayCB;

	private JButton optInOutCB;

	private JButton updateButton;

	int monday;
	int tuesday;
	int wednesday;
	int thursday;
	int friday;
	int saturday;
	int sunday;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1145, 75);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1129, 33);
		contentPane.add(panel);
		panel.setLayout(null);

		mondayCB = new JCheckBox("Monday");
		mondayCB.setHorizontalAlignment(SwingConstants.CENTER);
		mondayCB.setBounds(356, 5, 77, 23);
		panel.add(mondayCB);

		tuesdayCB = new JCheckBox("Tuesday");
		tuesdayCB.setHorizontalAlignment(SwingConstants.CENTER);
		tuesdayCB.setBounds(435, 5, 77, 23);
		panel.add(tuesdayCB);

		wednesdayCB = new JCheckBox("Wednesday");
		wednesdayCB.setHorizontalAlignment(SwingConstants.CENTER);
		wednesdayCB.setBounds(514, 5, 96, 23);
		panel.add(wednesdayCB);

		thursdayCB = new JCheckBox("Thursday");
		thursdayCB.setHorizontalAlignment(SwingConstants.CENTER);
		thursdayCB.setBounds(612, 5, 84, 23);
		panel.add(thursdayCB);

		fridayCB = new JCheckBox("Friday");
		fridayCB.setHorizontalAlignment(SwingConstants.CENTER);
		fridayCB.setBounds(698, 5, 69, 23);
		panel.add(fridayCB);

		saturdayCB = new JCheckBox("Saturday");
		saturdayCB.setHorizontalAlignment(SwingConstants.CENTER);
		saturdayCB.setBounds(769, 5, 77, 23);
		panel.add(saturdayCB);

		sundayCB = new JCheckBox("Sunday");
		sundayCB.setHorizontalAlignment(SwingConstants.CENTER);
		sundayCB.setBounds(848, 5, 77, 23);
		panel.add(sundayCB);

		updateButton = new JButton("Update");
		updateButton.setBounds(931, 5, 79, 23);
		panel.add(updateButton);

		JLabel firstNameLabel = new JLabel("First Name");
		firstNameLabel.setHorizontalAlignment(SwingConstants.CENTER);
		firstNameLabel.setBounds(10, 9, 69, 14);
		panel.add(firstNameLabel);

		firstNameTF = new JTextField();
		firstNameTF.setBounds(89, 6, 86, 20);
		panel.add(firstNameTF);
		firstNameTF.setColumns(10);

		JLabel surnameLabel = new JLabel("Surname");
		surnameLabel.setHorizontalAlignment(SwingConstants.CENTER);
		surnameLabel.setBounds(185, 9, 69, 14);
		panel.add(surnameLabel);

		surnameTF = new JTextField();
		surnameTF.setBounds(264, 6, 86, 20);
		panel.add(surnameTF);
		surnameTF.setColumns(10);
		updateButton.addActionListener(this);

		optInOutCB = new JButton("Opt In/Out");
		optInOutCB.setHorizontalAlignment(SwingConstants.CENTER);
		optInOutCB.setBounds(1020, 5, 99, 23);
		panel.add(optInOutCB);
		optInOutCB.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		DAO dao = new DAO(connection,statement,resultSet);
		Object target = e.getSource();
		boolean optInOut = false;
		String firstName = firstNameTF.getText();
		String surname = surnameTF.getText();
		if (target == updateButton) {
			monday = 0;
			tuesday = 0;
			wednesday = 0;
			thursday = 0;
			friday = 0;
			saturday = 0;
			sunday = 0;
			if (mondayCB.isSelected())
				monday = 1;
			if (tuesdayCB.isSelected())
				tuesday = 1;
			if (wednesdayCB.isSelected())
				wednesday = 1;
			if (thursdayCB.isSelected())
				thursday = 1;
			if (fridayCB.isSelected())
				friday = 1;
			if (saturdayCB.isSelected())
				saturday = 1;
			if (sundayCB.isSelected())
				sunday = 1; 

			try {
				dao.updateCustomerDaysOfWeek(firstName, surname, monday, tuesday, wednesday, thursday, friday, saturday,
						sunday);

			} catch (exceptionHandler e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
		if (target == optInOutCB) {
			mondayCB.setSelected(false);
			tuesdayCB.setSelected(false);
			wednesdayCB.setSelected(false);
			thursdayCB.setSelected(false);
			fridayCB.setSelected(false);
			saturdayCB.setSelected(false);
			sundayCB.setSelected(false);
			optInOut = true;
			try {
				dao.customerOptInOut(optInOut,firstName,surname);
			} catch (exceptionHandler e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	} 
}
