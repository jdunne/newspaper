package exceptionHandling;

public class EmployeeIDExceptionHandler  extends Exception 
{
		String message;

		public EmployeeIDExceptionHandler(String message) {
			this.message = message;
		}

		public String getMessage() {
			return message;
		}
}
